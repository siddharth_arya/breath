# User Interface
-------------------------------------
![Main control panel](./imgs/mcp.png)

### 1. Switching on

> The main switch is located next to the power port on the side of the machine.

![](./imgs/backside.png)

<iframe width="560" height="315" src="https://www.youtube.com/embed/c3seJixwfFc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### 2. Status screen

> When the ventilator boots up/powers up, the display is on the status screen which displays the following:
> * **TV** - *Tidal volume*
> * **FiO<sub>2</sub>** - *Fraction of Inspired Oxygen*
> * **PEEP** - *Positive End Expiratory Pressure*
> * **BPM** - *Breaths Per Minute*
> * **IER** - *Inspiration Expiration Ratio*
> * **IP** -
> * **EP** -

![Status screen](./imgs/status.png)

### 3. Selecting mode of operation

> The mode selection switch can be toggled up for 'Auto mode', neutral for 'off' and down for 'Assist' mode.

![](./imgs/switch.png)

### 4. Adjusting TV, BPM,  FiO<sub>2</sub>, IER, PIP, 'O2 in'

>  1. Press the **Rotary Dial** to enter the edit menu

![](./imgs/editmenu.png)

>  2. Rotate the **Rotary Dial** to move the cursor to the desired parameter and press the **Rotary Dial** to confirm selection

![](./imgs/editmenu1.png)

>  3. Rotate the appropriate **Adjustment Dial** to set a new value and rotate the **Rotary Dial** to either **SAVE** to save new value or **CANCEL** to keep the values unchanged  

![](./imgs/newval.png)

### 5. Start ventilation

> Press the 'Start/Stop' button to begin ventilation
