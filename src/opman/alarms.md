# Alarms

Alarms are expressed in 2 ways in the machine, an **audible buzzer** and **visual text** on the display.
The following alarms are available in P1.

> ### P1 Alarm table
| Description                                                       | Text on Display                                              | Alarm Priority        | Type of Alarm            |
|-------------------------------------------------------------------|--------------------------------------------------------------|-----------------------|--------------------------|
| When machine gets switched off during CMV                         | POWER FAILURE \| Check electricity supply                    | 100                   | Display, Buzzer_Critical |
| When electrical supply fails                                      | POWER FAILURE \| Check electricity supply                    | 125                   | Display, Buzzer_Critical |
| When Patient circuit disconnects                                  | PATIENT DISCONNECTED \| Check breath circuit                 | 200                   | Display, Buzzer_Critical |
| When inspiratory pressure or PEEP values are not achieved         | Pinsp NOT ACHIEVED / PEEP NOT ACHIEVED                       | 225                   | Display, Buzzer_moderate |
| When Bag Valve or Oxygen inlet is not providing supply            | AIR SUPPLY FAILURE \| Check BVM, breath circuit              | 250                   | Display, Buzzer_Critical |
| If airway pressure during inspiration exceeds Peak insp setting   | Pinsp EXCEEDED \| Peak insp pressure exceeded                | 500                   | Display, Buzzer_Moderate |
| If Tidal Volume is not achieved or exceeds                        | TV NOT ACHIEVED \| Check pressure limit, BVM, breath circuit | 600                   | Display, Buzzer_Moderate |
| Apnea (If patient is not breathing atleast once every 15 seconds) | LOW RR \| Patient resp rate under 4 bpm                      |                       |                          |
