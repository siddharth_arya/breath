# Summary


[Preface](./preface.md)
[Glossary](./gloss.md)
[List of Illustrations](./loi.md)
[Acknowledgements](./ack.md)

* [Introduction](./intro/intro.md)
  * [Overview](./intro/intro.md#overview)
  * [Purpose and Scope](./intro/intro.md#purpose-and-scope)
  * [Partners](./intro/intro.md#partners)
  * [Team](./intro/intro.md#team)
  * [Disclaimers](./intro/intro.md#disclaimers)
* [Design Philosophy](./dp/dp.md)
* [Specifications](./specs/specs.md)
  * [For Operators](./specs/specs.md#for-operators)
  * [For Developers](./specs/specs.md#for-developers)
* [Operation Manual](./opman/opman.md)
