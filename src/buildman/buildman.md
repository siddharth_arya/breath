# Build Manual

Build Manual contains information such as schematics, details of asemblies and subassemblies for manufacturers to build and manufacture this mechanical ventilator design.

Build manual is divided into the following sections which relate to the major components in the ventilator.

* Ambu-Bag mechanism
* Frame
  * Aluminium extrusion variant
  * Sheet metal body variant
* Gas circuits
  * Gas inlet circuits
  * Inspiratory circuit
  * Expiratory circuit
* Electrical schematic
* Electronics
  * Display board
  * Controller board
  * Oxygen sensor board
  * Pressure sensor boards (x2)

## Releases

All schematics are released in these versioned bundles,
