# Introduction

This is an Open Source Low-cost mechanical ventilator designed to address the current shortage of mechanical ventilators in the world caused due to the ongoing COVID19 pandemic. It is developed by **T-Works Foundation** (Government of Telangana) in collaboration with hardware, software and design professionals from the Hyderabad Hardware Community.

## Documentation overview

This document contains instructions to operate the machine ([Operation Manual](./opman/opman.md)), instructions to manufacture the ventilator ([Build Manual](./buildman/buildman.md)) and design theory for developers to contribute to or fork the ventilator design.

## Purpose and scope

The scope of this mechinical ventilator is limited to use in scenarios where commercial ventilators are not available or feasible, it does not replace a commercial ventilator completely.

## Partners

The following organisations were partners in this endevour, they contributed through provding manpower and valuable insights.

* **Honeywell**
* **Qualcomm**
* **NIMS**

## Team

This project was made possible from each of the following member's valuable contributions of time, expertise and effort.

| Name              | Organisation            |
|-------------------|-------------------------|
| Anand Rajagopalan | TWorks                  |
| Firoz Ahammad     | TWorks                  |
| Harshal Choudhary | Volunteer               |
| Kanishka Shah     | Entesla                 |
| Madhav Tenniti    | Spectrochem Instruments |
| Nagaraju          | Honeywell               |
| Rahul             | ConserVision            |
| Rajnikanth        | ConserVision            |
| Ravindra          | Qualcomm                |
| Rohan Rege        | TWorks                  |
| Shakeel Baig      | Qualcomm                |
| Sharat Reddy      | TWorks                  |
| Siddharth Arya    | TWorks                  |
| Simran Wasu       | TWorks                  |
| Srikanth Kumar    | TWorks                  |
| Sujai Karampuri   | TWorks                  |
| Surya Rao         | Althion                 |

*With special thanks to*
* Viplov (Trishula),
* Karna (Trishula),
* Anjan (TWorks) and
* Vinay (Bang design)

## Disclaimers

### Usage

The use of this Low cost ventilator should be limited to trained personnel, serious harm can be casued otherwise.

### Limitation of usage

Whilst this low-cost ventilator has been designed to be safe, it does not fully replace a commercially available FDA approved ventilator. This ventilator must be used in cases of last resort where no other equipment is available.

### Limitation of liabilities

While utmost care has been taken to accommodate patient and operator safety,
T-Works Foundation shall not be held responsible for any harm caused directly or indirectly by the use/misue of this ventilator.
