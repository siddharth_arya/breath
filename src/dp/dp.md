# Design Philosophy

* **Open source:** All the design files will be open sourced to enable contributions and ease of access, and adequate documentation will be created.

* **Plug and play:** The ventilator is to be of a plug and play nature, so as to enable clinicians to rapidly learn the operations of the ventilator and quickly deploy it.

* **Target use:** The ventilator is to be designed to cater to adults with indications of ARDS caused by COVID-19 (older adults weighing 35-120 kg are at higher risk)  

* **Functionality:** Assisted and autonomous ventilation modes to be supported

* **Modular design:** The design(s) will be modular allowing different modules to be mixed and matched depending on local availability

* **Biological and safety standards:** All components in the gas pathway meet biological safety and oxygen safety standards

* **Ease of Maintenance:** Highly serviceable, easy to maintain, with local sourcing

* **Ease of manufacturability:** Design will be inexpensive to build and manufactured locally (Hyderabad)

* **Ease of use:** Easy to use by hospital staff with minimal training.

* **Ease of access and replication:** Dissemination of designs via Internet through official portal of T-Works Foundation (tworks.telangana.gov.in)
